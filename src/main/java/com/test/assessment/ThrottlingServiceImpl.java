package com.test.assessment;

import com.test.assessment.SlaService.SLA;

import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ThrottlingServiceImpl implements ThrottlingService {

    private static class UserRateChecker {
        private final Double minReqDurationNs;
        private long lastAccessNs;

        public UserRateChecker(SLA sla) {
            minReqDurationNs = 1.0 / sla.getRps() * 1e+9;
            lastAccessNs = 0;
        }

        public synchronized boolean checkAndUpdateRate() {
            long currentTime = System.nanoTime();
            boolean isAllowed = (currentTime - lastAccessNs) >= minReqDurationNs;
            lastAccessNs = currentTime;
            return isAllowed;
        }
    }

    private static final int CACHE_RESET_PERIOD_HOURS = 24;
    private static final String GUEST_USER_ID = "GUEST" + UUID.randomUUID().toString();

    private final SLA guestSla;
    private SlaService slaService;
    private final ScheduledExecutorService timerService = Executors.newSingleThreadScheduledExecutor();

    private Map<String, SLA> slaByTokenCache = new ConcurrentHashMap<>();
    private Map<String, UserRateChecker> ratesByUser = new ConcurrentHashMap<>();

    public ThrottlingServiceImpl(SlaService slaService, int guestRps) {
        this.slaService = slaService;

        guestSla = new SLA(GUEST_USER_ID, guestRps);
        ratesByUser.put(GUEST_USER_ID, new UserRateChecker(guestSla));

        startCleanupTask();
    }

    @Override
    public boolean isRequestAllowed(Optional<String> token) {
        SLA sla = token.map(this::getSla).orElse(guestSla);
        return checkIsRequestAllowed(sla);
    }

    private SLA getSla(String token) {
        SLA sla = slaByTokenCache.get(token);
        if(sla != null) {
            return sla;
        }

        querySla(token);
        return guestSla;
    }

    private void querySla(String token) {
        slaService.getSlaByToken(token)
                .thenApplyAsync((sla) -> addSla(token, sla));
    }

    private SLA addSla(String token, SLA sla) {
        slaByTokenCache.put(token, sla);
        ratesByUser.put(sla.getUser(), new UserRateChecker(sla));
        return sla;
    }

    private boolean checkIsRequestAllowed(SLA sla) {
        return ratesByUser.get(sla.getUser()).checkAndUpdateRate();
    }

    private void cleanSlaCache() {
        slaByTokenCache.clear();
    }

    private void startCleanupTask() {
        timerService.submit(this::cleanSlaCache);
        timerService.scheduleAtFixedRate(this::cleanSlaCache,
                CACHE_RESET_PERIOD_HOURS,
                CACHE_RESET_PERIOD_HOURS,
                TimeUnit.HOURS);
    }
}
