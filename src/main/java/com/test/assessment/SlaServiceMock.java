package com.test.assessment;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class SlaServiceMock implements SlaService {

    private static final int POOL_SIZE = 4;
    private static final int RATE = 500;
    private static final int SERVICE_DELAY_MS = 300;

    private final ScheduledExecutorService timer = Executors.newScheduledThreadPool(POOL_SIZE);

    @Override
    public CompletableFuture<SLA> getSlaByToken(String token) {
        SLA sla = new SLA("USER-" + token, RATE);
        CompletableFuture<SLA> slaFuture = new CompletableFuture<>();
        scheduleCompletion(sla, slaFuture);
        return slaFuture;
    }

    private void scheduleCompletion(SLA sla, CompletableFuture<SLA> slaFuture) {
        timer.schedule(() -> slaFuture.complete(sla),
                SERVICE_DELAY_MS,
                TimeUnit.MILLISECONDS);
    }
}
