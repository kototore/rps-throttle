package com.test.assessment;

import java.util.Optional;

public interface ThrottlingService {
    boolean isRequestAllowed(Optional<String> token);
}
