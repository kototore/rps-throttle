package com.test.assessment;

import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.LongStream;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


public class ThrottlingServiceImplTest
{

    private static final int GUEST_RPS = 20;
    private static final long GUEST_ALLOWED_PERIOD_MS = 100;
    private static final long GUEST_DISALLOWED_PERIOD_MS = 10;
    private static final long USER_ALLOWED_PERIOD_MS = 10;
    private static final long USER_DISALLOWED_PERIOD_MS = 1;

    private ThrottlingService service;


    @Before
    public void initService() {
        service = new ThrottlingServiceImpl(new SlaServiceMock(), GUEST_RPS);
    }


    @Test
    public void shouldLimitGuest()
    {
        assertFalse(checkRequests(GUEST_DISALLOWED_PERIOD_MS, Optional.empty()));
    }

    @Test
    public void shouldAllowGuest()
    {
        assertTrue(checkRequests(GUEST_ALLOWED_PERIOD_MS, Optional.empty()));
    }

    @Test
    public void shouldAllowLazyInitUser() {
        final Optional<String> token = Optional.of(UUID.randomUUID().toString());

        // user has guest role
        assertFalse(checkRequests(USER_ALLOWED_PERIOD_MS, token));

        sleep(300); // SLA fetch time

        assertTrue(checkRequests(USER_ALLOWED_PERIOD_MS, token));
        assertFalse(checkRequests(USER_DISALLOWED_PERIOD_MS, token));
    }

    @Test
    public void shouldHasLowLatency() {
        int reqCount = 1000;
        long totalDuration = LongStream.range(0, reqCount).map(v -> {
            long start = System.nanoTime();
            service.isRequestAllowed(Optional.of("USER-" + v));
            return System.nanoTime() - start;
        }).sum();
        long avgDuration = totalDuration / 1000;

        // if REST gateway response time is 5 ms
        // and THROTTLING response must be << REST resp
        // then THROTTLING response time may be < 5 ms / 100 == ~ 500000 ns
        assertTrue(avgDuration < 500000);
    }


    // helpers
    private void sleep(long ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {}
    }

    private boolean checkRequests(long period, Optional<String> optToken) {
        final int N_REQUESTS = 10;
        List<Boolean> results = Collections.nCopies(N_REQUESTS, false);
        return results.stream()
                .map((v) -> {
                    sleep(period);
                    return service.isRequestAllowed(optToken);
                })
                .allMatch(v -> v);
    }


}
